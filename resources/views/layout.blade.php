<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
  </head>
  <body>

    <div class="container">

      <div class="pager-header">
        <h2>Cabeçalho da página</h2>
        <p>Defina o conteúdo dentro do @@section('content') e @@endsection para poder ser exibido</p>
      </div>

      @yield('content')

      <footer class="footer">
        <p>$copy; 2017 Curso de Verão IME USP</p>
      </footer>

    </div>
    <script type="text/javascript" src="/js/app.css">

    </script>
  </body>
</html>

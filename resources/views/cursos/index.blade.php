  @extends('layout')

  @section('content')
    <div class="row">
      <div class="col-md-5 col-md-offset-4">

        <h2>Cursos</h2>

        <ul class="list-group">
          @foreach ($cursos as $curso)
            <li class="list-group-item"><a href="/cursos/{{ $loop->index }}">{{ $curso }}</a></li>
          @endforeach
        </ul>

      </div>
    </div>

  @endsection

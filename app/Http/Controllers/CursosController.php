<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CursosController extends Controller
{

    private $cursos = array(
      'Lógica de programação', 'Python',
      'Desenvolvimento web com PHP', 'Java'
    );

    public function index(){
      $cursos = $this->cursos;
      return view('cursos.index', compact('cursos'));
    }

    public function show($index){
      $curso = $this->cursos[$index];
      return view('cursos.show', compact('curso'));
    }
}
